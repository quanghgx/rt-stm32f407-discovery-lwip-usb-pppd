/*
    ChibiOS - Copyright (C) 2006..2015 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 */

#include <string.h>
#include "ch.h"
#include "hal.h"
#include "usbcfg.h"
#include "lwip/api.h"
#include "chprintf.h"
#include "lwip/tcpip.h"
#include "netif/ppp/pppos.h"

/* For logging and modem communication*/
#define CRLF       "\r\n"
#define MODEM_SD     SDU1
#define TRACE_SD     SD3

/* Lwip stack global variables*/
static mutex_t trace_mutex;
static ppp_pcb * g_ppp;
static struct netif g_pppos_netif;

/* Prototypes*/
static void logger_init(void);
static void trace(const char* format, ...);

static void lwip_ipstack_init(void);
static void ppp_link_status_callback(ppp_pcb *pcb, int err_code, void *ctx);
static u32_t ppp_output_callback(ppp_pcb *pcb, u8_t *data, u32_t len, void *ctx);
static void netif_status_callback(struct netif *nif);
static void tcpip_init_done_callback(void *arg);
static void init_pppos_netif(void);
static void init_ok_callback(void);
static void sleep(uint8_t second);

/*
 * This is a periodic thread that does absolutely nothing except flashing
 * a LED.
 */
static THD_WORKING_AREA(waThread1, 128);

static THD_FUNCTION(Thread1, arg) {

    (void) arg;
    chRegSetThreadName("blinker");
    while (true) {
        palSetPad(GPIOD, GPIOD_LED3); /* Orange.  */
        chThdSleepMilliseconds(500);
        palClearPad(GPIOD, GPIOD_LED3); /* Orange.  */
        chThdSleepMilliseconds(500);
    }
}

/*
 * PPP input thread
 */
static THD_WORKING_AREA(waPppInputThread, 1024);

static THD_FUNCTION(PppInputThread, arg) {
    (void) arg;

    chRegSetThreadName("pppos_rx_thread");
    u32_t len;
    u8_t buffer[256];

    if (g_ppp) {
        trace("lwip (step 3.1/4): pppos_rx_thread stated" CRLF);

        /* Please read the "PPPoS input path" chapter in the PPP documentation. */
        while (true) {
            len = chnReadTimeout(&MODEM_SD, buffer, sizeof (buffer), MS2ST(1000));
            if (len > 0) {
                /* Pass received raw characters from PPPoS to be decoded through lwIP
                 * TCPIP thread using the TCPIP API. This is thread safe in all cases
                 * but you should avoid passing data byte after byte. */
                pppos_input_tcpip(g_ppp, buffer, len);
            }
        }
    } else {
        trace("lwip error (step 3.1/4): ppp_pcb need initialization" CRLF);
    }
}

/*
 * TCP echo server
 */
static THD_WORKING_AREA(waTcpEchoThread, 1024);

#define BUFLEN  200
#define MQTT_IP "192.168.141.227"

static THD_FUNCTION(TcpEchoThread, arg) {
    (void) arg;

    trace("enter echo thread" CRLF);
    sleep(5);

    chRegSetThreadName("tcpecho_thread");

    ip_addr_t target_ip;
    IP4_ADDR(&target_ip, 192, 168, 141, 227);

    char buf[] = "my payload from build (" __FILE__ " on " __DATE__ ":" __TIME__ ")";

    while (true) {
        trace("mqtt: [V4]try connect to " MQTT_IP ":4000..." CRLF);
        struct netconn* conn = netconn_new(NETCONN_TCP);

        if (conn) {
            trace("netconn_connect...." CRLF);
            sleep(5);
            err_t err = netconn_connect(conn, &target_ip, 4000);
            if (err == ERR_OK) {
                trace("mqtt: connected to " MQTT_IP ":4000" CRLF);

                while (netconn_write(conn, buf, strlen(buf), NETCONN_COPY) == ERR_OK) {
                    trace("mqtt: netconn_write ok for mqtt-connect with len: %d" CRLF, strlen(buf));
                    chThdSleepMilliseconds(100);
                }

                trace("mqtt-error: mqtt-connect with len: %d" CRLF, strlen(buf));

                /* Close connection and discard connection identifier. */
                netconn_close(conn);
            } else {
                trace("mqtt-error: cannot connect to " MQTT_IP ":4000, sleep 5 seconds, error: %d" CRLF, err);
                sleep(5);
            }
            netconn_delete(conn);
            sleep(1);
        } else {
            trace("mqtt-error: cannot create netconn, sleep 5 seconds" CRLF);
            sleep(1);
        }
    }
}

/*
 * Application entry point.
 */
int main(void) {

    /*
     * System initializations.
     * - HAL initialization, this also initializes the configured device drivers
     *   and performs the board-specific initializations.
     * - Kernel initialization, the main() function becomes a thread and the
     *   RTOS is active.
     */
    halInit();
    chSysInit();

    /* Initializing logger*/
    logger_init();

    /*
     * Creates the example thread.
     */
    chThdCreateStatic(waThread1, sizeof (waThread1), NORMALPRIO - 3, Thread1, NULL);

    /* Start ip stack*/
    lwip_ipstack_init();

    /*
     * Normal main() thread activity, in this demo it does nothing except
     * sleeping in a loop and check the button state.
     */
    while (true) {
        sleep(10);
    }
}

/* Implementations*/
static void logger_init(void) {
    chMtxObjectInit(&trace_mutex); /* Initializing the mutex sharing the resource.*/

    SerialConfig trace_sd_config = {115200, 0, USART_CR2_STOP1_BITS | USART_CR2_LINEN, 0};
    sdStart(&TRACE_SD, &trace_sd_config); /* TRACE_SD for debug, and console print*/
    palSetPadMode(GPIOD, 8, PAL_MODE_ALTERNATE(7));
    palSetPadMode(GPIOD, 9, PAL_MODE_ALTERNATE(7));
}

static void trace(const char* format, ...) {
    va_list ap;
    va_start(ap, format);
    chMtxLock(&trace_mutex);
    chvprintf((BaseSequentialStream*) & TRACE_SD, format, ap);
    chMtxUnlock(&trace_mutex);
    va_end(ap);
}

static void lwip_ipstack_init(void) {

    trace("lwip (step 0/4): init usart port on MODEM_SD" CRLF);
    /*
     * Initializes a serial-over-USB CDC driver.
     */
    sduObjectInit(&MODEM_SD);
    sduStart(&MODEM_SD, &serusbcfg);

    /*
     * Activates the USB driver and then the USB bus pull-up on D+.
     * Note, a delay is inserted in order to not have to disconnect the cable
     * after a reset.
     */
    usbDisconnectBus(serusbcfg.usbp);
    sleep(2);
    usbStart(serusbcfg.usbp, &usbcfg);
    usbConnectBus(serusbcfg.usbp);

    /* Init modem*/
    while (MODEM_SD.config->usbp->state != USB_ACTIVE) {
        trace("wait until USB to Serial is ready..." CRLF);
        sleep(5);
    }

    trace("lwip (step 1/4): start initialization flow. (build %s-%s)" CRLF, __DATE__, __TIME__);
    tcpip_init(tcpip_init_done_callback, NULL);
}

static void tcpip_init_done_callback(void *arg) {
    LWIP_UNUSED_ARG(arg);

    trace("lwip (step 2/4): TCP/IP initialized, now start netifs" CRLF);
    init_pppos_netif();

    /* Start PPPoS receiver thread */
    trace("lwip (step 3.1/4): TCP/IP initialized, now start PPPoS receiver thread" CRLF);
    chThdCreateStatic(waPppInputThread, sizeof (waPppInputThread), NORMALPRIO - 1, PppInputThread, NULL);
}

static void init_pppos_netif(void) {
    g_ppp = pppos_create(&g_pppos_netif, ppp_output_callback, ppp_link_status_callback, NULL);
    if (g_ppp) {
        trace("lwip (step 3.2/4): request pppos_create successful, start connect" CRLF);
        ppp_connect(g_ppp, 0);
        netif_set_status_callback(&g_pppos_netif, netif_status_callback);
        netif_set_default(&g_pppos_netif);
    } else {
        trace("lwip error (step 3.2/4): request pppos_create failed" CRLF);
    }
}

static void ppp_link_status_callback(ppp_pcb *pcb, int err_code, void *ctx) {
    LWIP_UNUSED_ARG(pcb);
    LWIP_UNUSED_ARG(ctx);

    if (err_code == PPPERR_NONE) /* No error. */ {
        trace("lwip (step 4.2/4): ppp_link_status_cb: PPPERR_NONE" CRLF);
        trace("lwip (step 4.3/4): start custom applications" CRLF);
        init_ok_callback();
    } else {
        trace("lwip error (step 4.2/4): ppp_link_status_cb: %d" CRLF, err_code);
    }
}

static u32_t ppp_output_callback(ppp_pcb *pcb, u8_t *data, u32_t len, void *ctx) {
    LWIP_UNUSED_ARG(pcb);
    LWIP_UNUSED_ARG(ctx);

    u32_t sended = 0;
    while (sended < len) {
        sended += chnWrite(&MODEM_SD, data + sended, len - sended);
    }
    return sended;
}

static void netif_status_callback(struct netif *nif) {
    trace("lwip (step 4.1/4): netif %c%c%d: %s" CRLF,
            nif->name[0], nif->name[1], nif->num, netif_is_up(nif) ? "UP" : "DOWN");
    trace("IPV4: Host at %s ", ip4addr_ntoa(netif_ip4_addr(nif)));
    trace("mask %s ", ip4addr_ntoa(netif_ip4_netmask(nif)));
    trace("gateway %s" CRLF, ip4addr_ntoa(netif_ip4_gw(nif)));
}

static void init_ok_callback(void) {
    trace("ready to start application " CRLF);
    chThdCreateStatic(waTcpEchoThread, sizeof (waTcpEchoThread), NORMALPRIO - 2, TcpEchoThread, NULL);
}

static void sleep(uint8_t second) {
    chThdSleepMilliseconds(second * 1000);
}
