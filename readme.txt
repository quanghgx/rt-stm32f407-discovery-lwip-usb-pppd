*****************************************************************************
** ChibiOS/RT port for ARM-Cortex-M4 STM32F407.                            **
*****************************************************************************

** TARGET **

The demo runs on an ST STM32F4-Discovery board.

** The Demo **
Lwip 2.0 integration with PPP plugin.
Using either PPPD (from linux) or a GPRS modem. For example SIM COM 900A (already 
tested with QUECTEL M95, Sierra Wireless HL6528, HL8518)

** Build Procedure **

The demo has been tested by using the free GNU ARM Toolchain on Launchpad [link](https://launchpad.net/gcc-arm-embedded). just modify 
the TRGT line in the makefile in order to use different GCC toolchains.

Launch GDB Server with ST-Link from Texane:
st-util -v99 -m

Launch PPPd Server:
sudo pppd file doc/options

** Reference **
[1]: https://lists.nongnu.org/mailman/listinfo/lwip-devel "LWIP developer and user mailing list"
[2]: http://www.dalbert.net/?p=259 "David Albert wonderful article about ChibiOS, PPP new and Linux PPPD"
[3]: https://github.com/MarioViara/gprs "Mario Viara GPRS library for LWIP PPP-new"

** Notes **

Some files used by the demo are not part of ChibiOS/RT but are copyright of
ST Microelectronics and are licensed under a different license.
Also note that not all the files present in the ST library are distributed
with ChibiOS/RT, you can find the whole library on the ST web site:

                             http://www.st.com
