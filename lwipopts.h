/**
 * @file
 *
 * lwIP Options Configuration
 */

/*
 * Copyright (c) 2001-2004 Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 *
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

/*
 * Copyright (c) 2013,2014,2015 2016 Embedded Integration Platform, Viettel Corporation
 * All rights reserved.
 * 
 * Author: quanghx2@viettel.com.vn
 *
 * Customization for viettel ARM-Cortex M3 board with M95 GSM modem.
 */

#ifndef LWIP_LWIPOPTS_H
#define LWIP_LWIPOPTS_H

/*
 * Include user defined options first. Anything not defined in these files
 * will be set to standard values. Override anything you don't like!
 */
#include "lwip/arch.h"

/* Custom configurations*/
#define LWIP_DEBUG          1
#define SIO_DEBUG           1

/* From the Contribution-repository configurations*/
#define NO_SYS                          0
#define MEM_ALIGNMENT                   4
#define MEM_SIZE                        2048/*1600*/
#define MEMP_NUM_RAW_PCB                3   /*4*/
#define MEMP_NUM_PBUF                   16
#define MEMP_NUM_TCP_PCB                64
#define MEMP_NUM_NETCONN                12

#define TCPIP_MBOX_SIZE                 MEMP_NUM_PBUF
#define DEFAULT_ACCEPTMBOX_SIZE         MEMP_NUM_PBUF  /*4*/
#define DEFAULT_RAW_RECVMBOX_SIZE       16  /*4*/
#define DEFAULT_UDP_RECVMBOX_SIZE       4
#define DEFAULT_TCP_RECVMBOX_SIZE       MEMP_NUM_PBUF  /*4*/

#define LWIP_ARP                        0
#define LWIP_IPV4                       1
#define IP_FORWARD                      0
#define IP_REASSEMBLY                   1
#define IP_FRAG                         1
#define LWIP_ICMP                       1
#define LWIP_RAW                        0
#define LWIP_DHCP                       0
#define LWIP_DNS                        0
#define LWIP_UDP                        0
#define LWIP_TCP                        1
#define LWIP_NETCONN                    1
#define LWIP_SOCKET                     0
#define LWIP_STATS                      1
#define LWIP_NETIF_API                  1
#define LWIP_NETIF_STATUS_CALLBACK      1
#define LWIP_IPV6                       0

#define LINK_STATS                      1
#define IP_STATS                        1
#define ICMP_STATS                      1
#define TCP_STATS                       1

#define PPP_SUPPORT                     1      /* Set > 0 for PPP */
#define MPPE_SUPPORT                    0
#define PPPOE_SUPPORT                   0
#define PPPOL2TP_SUPPORT                0
#define PPPOS_SUPPORT                   PPP_SUPPORT
#define LWIP_TCPIP_CORE_LOCKING         1

#define TCPIP_THREAD_PRIO               NORMALPRIO
#define TCPIP_THREAD_STACKSIZE          2048
#define DEFAULT_THREAD_PRIO             NORMALPRIO
#define DEFAULT_THREAD_STACKSIZE        1024

#endif /* LWIP_HDR_OPT_H */
